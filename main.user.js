// ==UserScript==
// @name         Evernote direct link opening, without "You are leaving Evernote"
// @namespace    http://tampermonkey.net/
// @version      1.0
// @author       Vitaly Zdanevich
// @match        https://www.evernote.com/client/web*
// @grant        none
// @description  Faster links opening. Git repo at https://gitlab.com/vitaly-zdanevich/evernote-direct-link-opening-without-you-are-leaving
// @supportURL   https://gitlab.com/vitaly-zdanevich-userscripts/evernote-direct-link-opening-without-you-are-leaving
// @license MIT
// ==/UserScript==

(function() {
    document.addEventListener('click', callback, false);
})()

function callback(e) {
    if (e.target.tagName !== 'A') {
        return;
    }

    e.preventDefault();
    const url = decodeURIComponent(e.target.href.match(/dest=(.*)/)[1])
    window.open(url, '_blank');
}
